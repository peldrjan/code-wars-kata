def dirReducComplex(arr):
    dir_to_complex = {
        "SOUTH": complex(1),
        "NORTH": complex(-1),
        "WEST": complex(0, 1),
        "EAST": complex(0, -1)
    }
    complex_to_dir = {v: k for k, v in dir_to_complex.items()}

    direction = sum([dir_to_complex[item.upper()] for item in arr])
    result = []
    if direction.imag > 0:
        result.append(complex_to_dir[complex(0, direction.imag)])
    if direction.real > 0:
        result.append(complex_to_dir[complex(direction.real, 0)])

    return result

def dirReduc(arr):
    direction_map = {
        "SOUTH": "NORTH",
        "WEST": "EAST"
    }
    direction_map.update({v: k for k, v in direction_map.items()})
    
    changed = True
    instructions = arr.copy()
    while changed:
        changed = False
        i = 0
        while i < len(instructions) - 1:
            if instructions[i] == direction_map[instructions[i + 1]]:
                instructions.pop(i)
                instructions.pop(i)
                changed = True
            i += 1

    return instructions

print(dirReduc(["NORTH", "SOUTH", "SOUTH", "EAST", "WEST", "NORTH", "WEST"]))