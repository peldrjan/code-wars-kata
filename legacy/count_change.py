def count_change(count, denominations):
    if count <= 0 or not denominations:
        return []
    
    largest_denomination = sorted(denominations, reverse=True)[0]
    counter = [[]]
    div, mod = divmod(count, largest_denomination)
    if mod == 0:
        if count - largest_denomination == 0:
            counter.append([largest_denomination])
        else:
            rest = count_change(count - largest_denomination, denominations)
            if len(rest) > 0:
                counter.append([largest_denomination] + rest)

    counter += count_change(count, [denomination for denomination in denominations if denomination != largest_denomination])

    return counter


#print(count_change(4, [1,2]))
print(count_change(10, [5,2,3]))
print(count_change(11, [5,7]))