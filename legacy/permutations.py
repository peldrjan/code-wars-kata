def permutations(string):
    def get_descendants(letters):
        if len(letters) == 1:
            return letters

        result = set() 
        for index, letter in enumerate(letters):
            for descendant in get_descendants(letters[0:index] + letters[index+1:]):
                result.add(letter + descendant)
        return result


    return list(get_descendants([letter for letter in string]))

print(permutations("aabc"))

    
