def snail(snail_map):
    result = []
    reduced_matrix = snail_map

    while reduced_matrix:
        result += reduced_matrix[0]
        reduced_matrix = reduced_matrix[1:]
        if not reduced_matrix:
            break

        result += [row[-1] for row in reduced_matrix]
        reduced_matrix = [row[:-1] for row in reduced_matrix]
        if not reduced_matrix:
            break

        result += reversed(reduced_matrix[-1])
        reduced_matrix = reduced_matrix[:-1]
        if not reduced_matrix:
            break

        result += reversed([row[0] for row in reduced_matrix])
        reduced_matrix = [row[1:] for row in reduced_matrix]

    return result

print(snail([[1,2,3], [4,5,6], [7,8,9]]))