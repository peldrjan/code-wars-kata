import numpy as np

def solution2(a):
    result = np.array(a)
    current_max = np.max(result)

    while True:
        if(not any(result[result < current_max])):
            break
        second_biggest = np.max(result[result < current_max])
        result[result > second_biggest] = result[result > second_biggest] - second_biggest
        current_max = np.max(result)
    
    return sum(result)

def solution3(a):
    result = sorted(a)
    while result[0] != result[-1]:
        for index in range(len(result) - 1):
            for sub_index in range(index + 1, len(result) - 1):
                if result[sub_index] > result[index]:
                    result[sub_index] = result[sub_index] - result[index]
        result = sorted(result)
    
    return sum(result)

def solution(a):
    result = np.array(a)
    result.sort()

    last_min = result[0]
    while len(result) > 0:
        last_min = result[0]
        result = result - np.floor_divide(result, result[0]) * result[0]
        result = result[result > 0]
        result.sort()

    return last_min * len(a)

#print(solution ([9]), 9)
#print(solution ([6, 9, 21]), 9)
#print(solution ([1, 21, 55]), 3)

#print(solution([1699689420, 8936159180, 6867605420, 2497248000, 7252467755, 1014897195, 6798757680, 10452066755, 624312000, 8016339500, 837661955, 874210220, 7181712395, 424922355, 5712844995, 1857718395, 1096057755, 9455248595, 3137211155, 433550000, 374977395, 2819158875, 359022755, 8662719195, 108387500, 1082314220, 3646155500, 2373946380, 4274629580, 6096796875, 172075995, 4006695680, 8585373875, 5250463920, 4301899875, 1948547120, 135961280, 4692788555, 1985485580, 206413155, 3646155500, 6867605420, 1581633755, 2098382000, 4439552000, 6031937795, 8128585595, 7006341420, 1467826880, 7323569955, 3621052955, 10240277580, 212439500, 755417520, 874210220, 1123804955, 2175380480, 4439552000, 6526835120, 4329256880, 1499909580, 6359701595, 5807662380, 4836510380, 5130457280, 4193338955, 2709687500, 263771820, 2313466155, 886566395, 10623058875, 257051795, 1614930395, 9414798380, 4750017155, 6359701595, 2841313280, 2602383875]))
#print(solution([13052446150, 36356882584, 63173839366, 77796924696, 82706879350, 116394296134, 85448358646, 148447751526, 15107934816, 2704230936]))
print(solution([15069038, 5580000, 1285632, 13988750, 1986542, 13579488, 4519800, 3782558, 5580000, 7083128]))