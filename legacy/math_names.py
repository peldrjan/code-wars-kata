def zero(function = None):
    return process_value(0, function)
def one(function = None):
    return process_value(1, function)
def two(function = None):
    return process_value(2, function)
def three(function = None):
    return process_value(3, function)
def four(function = None):
    return process_value(4, function)
def five(function = None):
    return process_value(5, function)
def six(function = None):
    return process_value(6, function)
def seven(function = None):
    return process_value(7, function)
def eight(function = None):
    return process_value(8, function)
def nine(function = None):
    return process_value(9, function)

def process_value(value, function):
    return value if function is None else function(value)

def plus(term2):
    return lambda term1: term1 + term2
def minus(term2):
    return lambda term1: term1 - term2
def times(term2):
    return lambda term1: term1 * term2
def divided_by(term2):
    return lambda term1: term1 // term2

print(eight(divided_by(three())))