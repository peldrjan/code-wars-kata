def determinant(matrix):
    if len(matrix) == 1:
        return matrix[0][0]
    elif len(matrix) == 2:
        return matrix[0][0] * matrix[1][1] - matrix[0][1] * matrix[1][0]
    else:
        result = 0
        sign = 1
        for index, value in enumerate(matrix[0]):
            result += sign * value * determinant([row[:index] + row[index+1:] for row in matrix[1:]])
            sign *= -1
        return result