def next_bigger(n):
    digits = [int(digit) for digit in str(n)]
    new_digits = [digits[-1]]

    added = False
    for digit in reversed(digits[0:-1]):
        if digit < max(new_digits) and not added:
            number_end = new_digits + [digit]
            first = min([number for  number in number_end if number > digit])
            number_end.remove(first)
            new_digits = sorted(number_end, reverse=True) + [first] 
            added = True
        else:
            new_digits.append(digit)
    
    result = int(''.join([str(digit) for digit in reversed(new_digits)]))
    
    return -1 if result == n else result

print(next_bigger(58385111))