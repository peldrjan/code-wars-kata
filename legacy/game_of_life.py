# -*- coding: utf-8 -*-
def htmlize(array):
    s = []
    for row in array:
        for cell in row:
            s.append('▓▓' if cell else '░░')
        s.append('\n')
    return ''.join(s)

import copy

def expand(cells):
    length = len(cells[0])
    return [[0] * (length + 2)] + [[0] + row + [0] for row in copy.deepcopy(cells)] + [[0] * (length + 2)]

def collapse(cells):
    while len(cells) > 0 and sum(cells[0]) == 0:
        cells = cells[1:]
    while len(cells) > 0 and sum(cells[-1]) == 0:
        cells = cells[:-1]
    while len(cells) > 0 and sum([row[0] for row in cells]) == 0:
        cells = [row[1:] for row in cells]
    while len(cells) > 0 and sum([row[-1] for row in cells]) == 0:
        cells = [row[:-1] for row in cells]
    return cells
        

def next_generation(cells):
    result = expand([[0] * len(row) for row in cells])
    extended = expand(expand(cells))

    for row_index in range(len(result)):
        for column_index in range(len(result[0])):
            neighbors = (sum(extended[row_index][column_index:column_index + 3]) + 
                extended[row_index + 1][column_index] + 
                extended[row_index + 1][column_index + 2] +
                sum(extended[row_index + 2][column_index:column_index + 3]))

            new_value = 0
            if 2 > neighbors > 3:
                new_value = 0
            elif extended[row_index + 1][column_index + 1] == 0 and neighbors == 3:
                new_value = 1
            elif extended[row_index + 1][column_index + 1] == 1 and 2 <= neighbors <= 3:
                new_value = 1

            result[row_index][column_index] = new_value

    return collapse(result)

def get_generation(cells, generations):
    generation = cells
    for _ in range(generations):
        generation = next_generation(generation)

    return generation

start = [[1,0,0],
         [0,1,1],
         [1,1,0]]
end   = [[0,1,0],
         [0,0,1],
         [1,1,1]]
         
print(htmlize(start))

print(htmlize(get_generation(start, 5)))