def dbl_linear(n):
    x = [1]
    y = []
    z = []
    for i in range(n + 1):
        y.append(2 * x[i] + 1)
        z.append(3 * x[i] + 1)

        if y[0] == z[0]:
            y.pop(0)
            x.append(z.pop(0))
        elif y[0] < z[0]:
            x.append(y.pop(0))
        else:
            x.append(z.pop(0))

        max_value = z

        if len(x) > n :
            return x[n]


print(dbl_linear(50))