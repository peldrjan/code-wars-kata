

import regex as re


def tokenize(expression):
    if expression == "":
        return []

    regex = re.compile("\s*(=>|[-+*\/\%=\(\)]|[A-Za-z_][A-Za-z0-9_]*|[0-9]*\.?[0-9]+)\s*")
    tokens = regex.findall(expression)
    return [s for s in tokens if not s.isspace()]


def get_identifier(text):
    tokens = tokenize(text)
    if len(tokens) != 1:
        return None
    else:
        return tokens[0] if re.fullmatch("^[a-zA-Z](_+(\d|[a-zA-Z]|_)*)*$", tokens[0]) else None


def is_assignment(text):
    return '=' in text


def is_valid_assignment(text):
    parts = text.split('=')
    left_side = tokenize(parts[0])
    return len(parts) == 2 and len(left_side) == 1 and get_identifier(left_side[0]) is not None


def get_sides(text):
    sides = text.split('=')
    return sides[0], sides[1]


def has_special_character(text):
    return False if not text else not re.match('^[^\+\-\*\/\%\=\)\(]+$', text)


def is_only_special_characters(text):
    return False if not text else re.match('^[\+\-\*\/\%\=\)\(]+$', text) is not None


def is_additive(text):
    return False if not text else not re.match('^[^\+\-]+$', text)


def valid_parentheses(string):
    opened = 0
    for item in string:
        if item == ')':
            if opened == 0:
                return False
            opened -= 1
        elif item == '(':
            opened += 1
    return opened == 0


def is_only_multiplicative(text):
    return False if not text else any(sign in text for sign in ['*', '/', '%']) and re.match('^[^\+\-\=\)\(]+$', text) is not None


def is_only_additive(text):
    return False if not text else any(sign in text for sign in ['-', '+']) and re.match('^[^\*\/\%\=\)\(]+$', text) is not None


def split_by_pattern(pattern, text):
    match_iter = re.finditer(pattern, text)
    parts = []
    last_index = 0
    for match in match_iter:
        if match.start() >= last_index:
            if last_index < match.start():
                parts.append(text[last_index:match.start()])
            parts.append(text[match.start():match.end()])
            last_index = match.end()

    if last_index != len(text):
        parts.append(text[last_index:])

    return parts


class Interpreter:
    def __init__(self):
        self.vars = {}
        self.functions = {}

    def __str__(self):
        return f"Stored variables:\n{str(self.vars)}"

    def _get_value(self, token):
        try:
            return int(token)
        except ValueError as e:
            identifier = get_identifier(token)
            if identifier is None:
                raise ValueError(f"Invalid indentifier format {token}")
    
            if identifier in self.vars:
                return self.vars[identifier]
            else:
                raise ValueError(f"Unknown identifier {identifier}")
    
    def _evaluate_operation(self, expression, operations):
        tokens = tokenize(expression)
        value = self._get_value(tokens[0])
        method = None
        for index in range(1, len(tokens) - 1, 2):
            value = operations[tokens[index]](value, self._get_value(tokens[index+1]))
        return value
    
    def _evaluate_multiplication(self, expression):
        if not is_only_multiplicative(expression):
            raise ValueError(f"Expression is not multiplicative: {expression}")
        return self._evaluate_operation(expression, {
                "*": lambda x, y: x * y,
                "/": lambda x, y: x // y,
                "%": lambda x, y: x % y,
                })
    
    def _evaluate_addition(self, expression):
        if not is_only_additive(expression):
            raise ValueError(f"Expression is not additive: {expression}")
        return self._evaluate_operation(expression, {
                "+": lambda x, y: x + y,
                "-": lambda x, y: x - y,
                })

    def _evaluate_expression(self, expression):
        if not valid_parentheses(expression):
            raise ValueError(f"Invalid parentheses in expression {expression}")
    
        if is_only_special_characters(expression):
            raise ValueError(f"Invalid expression {expression}")
    
        if not has_special_character(expression):
            value_tokens = tokenize(expression)
            if len(value_tokens) > 1:
                raise ValueError(f"Invalid expression {expression}")
    
            return self._get_value(value_tokens[0])
    
        if is_only_multiplicative(expression):
            return self._evaluate_multiplication(expression)
    
        no_bracket_expression = self._evaluate_brackets(expression)
    
        if expression != no_bracket_expression:
            return self._evaluate_expression(no_bracket_expression)
        else:
            return self._evaluate_addition(self._evaluate_additive(expression))

    def _evaluate_brackets(self, text):
        parts = split_by_pattern('\((?:[^()]+|(?R))*+\)', text)
        return ''.join([str(self._evaluate_expression(part[1:-1])) if part[0] == '(' and part[-1] == ')' else part for part in parts])

    def _evaluate_additive(self, text):
        return ''.join([str(self._evaluate_expression(part)) if part not in ['-', '+'] else part for part in split_by_pattern('(\+|\-)', text)])
    
    def input(self, expression):
        if not expression.strip():
            return ''
        
        if is_assignment(expression):
            if not is_valid_assignment(expression):
                raise ValueError(f"Not a valid assignment: {expression}")
            else:
                left_side, right_side = get_sides(expression)
                identifier = get_identifier(left_side)
                if identifier is None:
                    raise ValueError(f"Not a valid identifier: {left_side}")
    
                value = self._evaluate_expression(right_side)
                self.vars[identifier] = value
                return value
        else:
            return self._evaluate_expression(expression)

interpreter = Interpreter()

# Basic arithmetic
print(interpreter.input("1 + 1"), 2)
print(interpreter.input("2 - 1"), 1)
print(interpreter.input("2 * 3"), 6)
print(interpreter.input("8 / 4"), 2)
print(interpreter.input("7 % 4"), 3)

# Variables
print(interpreter.input("x = 1"), 1)
print(interpreter.input("x"), 1)
print(interpreter.input("x + 3"), 4)
try:
    interpreter.input("y")
    print("mistake")
except:
    print("valid error")
