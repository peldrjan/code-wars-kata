from datetime import timedelta

def format_duration(seconds):
    unit_map = {
        "year": 365 * 24 * 60 * 60,
        "day": 24 * 60 * 60,
        "hour": 60 * 60
        "minute": 60,
        "second": 1
    }

    unit_counts = {}

    remaining = seconds
    for unit, duration in unit_map.items():
        if remaining >= duration:
            unit_count = remaining // duration
            unit_text = unit if unit_count == 1 else unit + "s"
            unit_counts[unit_text] = unit_count
            remaining = remaining % duration

    return {
        1: "{}",
        2: "{} and {}",
        3: "{}, {} and {}",
        4: "{}, {}, {} and {}"
    }[len(unit_counts)].format(*[f"{unit_count} {unit}" for unit, unit_count in unit_counts.items()])

print(format_duration(2 * 365 * 24 * 60 * 60 + 2))