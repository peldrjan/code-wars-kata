import re

def solution(text, comment_chars):
    no_comment = lambda line, delimiters: line if not delimiters else re.split(delimiters, line)[0]

    delimiters = "|".join([re.escape(delimiter) for delimiter in comment_chars])
    return "\n".join([no_comment(line, delimiters).rstrip() for line in text.split("\n")])


print(solution("apples, pears $ and bananas\ngrapes\nbananas !apples", ["$", "!"]))
print(solution("abs", []))