def longest_slide_down(pyramid):
    if not any(pyramid):
        return 0

    previous = [0, 0]
    for store in pyramid:
        current = [item + max(previous[index:index+2]) for index, item in enumerate(store)]
        print(current)
        previous = [0] + current + [0]
    return max(previous)
        

print(longest_slide_down([[3], [7, 4], [2, 4, 6], [8, 5, 9, 3]]))