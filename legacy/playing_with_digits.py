def dig_pow(n, p):
    digit_sum = sum([int(digit)**power for power, digit  in enumerate(str(n), p)])
    division = digit_sum / n
    if division.is_integer():
        return int(division)
    else:
        return -1

def sort_array(source_array):
    odd = sorted([item for item in source_array if not item % 2 == 0])
    return [item if item % 2 == 0 else odd.pop(0) for item in source_array]
    
print(sort_array([5, 3, 2, 8, 1, 4]))